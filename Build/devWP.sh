#!/bin/bash

# File Name:    devWP.sh
# Description:  Build a fresh WordPress appliance for use with one site in development
# Version:      1
# Auther:       AdminDogg
# Date:         27/01/18


#    _____             _    ___ _             _
#   |_   _|__  __ _ __| |_ / __| |___ _  _ __| |
#     | |/ _ \/ _` (_-<  _| (__| / _ \ || / _` |
#     |_|\___/\__,_/__/\__|\___|_\___/\_,_\__,_|
#                    | |__ _  _
#                    | '_ \ || |
#                    |_.__/\_, |
#      _      _       _    |__/_
#     /_\  __| |_ __ (_)_ _ |   \ ___  __ _ __ _
#    / _ \/ _` | '  \| | ' \| |) / _ \/ _` / _` |
#   /_/ \_\__,_|_|_|_|_|_||_|___/\___/\__, \__, |
#                                     |___/|___/


# constants
# Set dev directory
DevDIR="/home/ricky/dev"

# Wordpress database password
WPDBPassword="WPDBsecure"

# Set MySQL root password
MySQLrootPW="MySQLrootSecure"


#Check all used variables to see if they have been set
if [ "$DBName" -eq "" ]; then
  echo "DBName not set"
  exit 1
fi

if [ "$HostName" -eq ""]; then
  echo "HostName not set"
  exit 1
fi

echo "Drop backup sql file directly into $DevDir"
echo "Drop zip containing the www root directory into $DevDir (full path from source server will be assumed)"

# Prompt for backup files to be staged
read -p "Press any key to continue, or Ctrl+C to abort"

# Check how many SQL files in dev directory
NumberOfSQL=ls -1 | grep *.sql | wc -l

if [ "$NumberOfSQL" -gt 1 ]; then
  echo "$NumberOfSQL files in dev directory.  Remove all but the one to restore and try again"
  exit 1
fi

# Set variable used later to restore database
sqlFile=ls $DevDir | grep *.sql

BackupFile="$DevDir/$sqlFile"

# Wordpress database username
WPUser=WP_$HostName

# Set server hostname
OldHostname=$(cat /etc/hostname)

sudo sed -i "s/$OldHostname/$HostName/g" /etc/hosts
sudo sed -i "s/$OldHostname/$HostName/g" /etc/hostname

# Install Apache, php, zip and Wordpress required php modules
sudo apt-get --yes install apache2 php libapache2-mod-php php-mcrypt php-mysql \
php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc zip

# Add the ServerName and .htaccess overrides to the Apache config file
echo "\n\nServerName $HostName\n\n" | sudo tee -a /etc/apache2/apache2.conf
echo "<Directory /var/www/html/>
    AllowOverride All
</Directory>" | sudo tee -a /etc/apache2/apache2.conf
sudo systemctl restart apache2

# Open enable Apache firewall rules
sudo ufw allow in "Apache Full"

# Amend apache configuration to prefer php files over html files
sudo sed -i "s/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm/g" /etc/apache2/mods-enabled/dir.conf
sudo systemctl restart apache2

# Install MySQL
export DEBIAN_FRONTEND=noninteractive
echo mysql-server mysql-server/root_password select $MySQLrootPW | sudo debconf-set-selections
echo mysql-server mysql-server/root_password_again select $MySQLrootPW | sudo debconf-set-selections
sudo apt-get install -y mysql-server

# Restore database
# Create db for site
mysql -u root -p$MySQLrootPW -e "CREATE DATABASE $DBName DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL ON $DBName.* TO '$WPUser'@'localhost' IDENTIFIED BY '$WPDBPassword';
FLUSH PRIVILEGES;"

# Add line to MySQL dump file to use current site's db
sed -i "USE $DBName" $BackupFile

# Run import
mysql -u $WPUser -p$WPDBPassword < $BackupFile
