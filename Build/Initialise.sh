#!/bin/bash

# File Name:    Innitialise.sh
# Description:  Fresh ToastCloud server automated build
# Version:      1
# Auther:       AdminDogg
# Date:         28/01/18


#    _____             _    ___ _             _
#   |_   _|__  __ _ __| |_ / __| |___ _  _ __| |
#     | |/ _ \/ _` (_-<  _| (__| / _ \ || / _` |
#     |_|\___/\__,_/__/\__|\___|_\___/\_,_\__,_|
#                    | |__ _  _
#                    | '_ \ || |
#                    |_.__/\_, |
#      _      _       _    |__/_
#     /_\  __| |_ __ (_)_ _ |   \ ___  __ _ __ _
#    / _ \/ _` | '  \| | ' \| |) / _ \/ _` / _` |
#   /_/ \_\__,_|_|_|_|_|_||_|___/\___/\__, \__, |
#                                     |___/|___/


# constants
RunPath=/home/ricky/server-scripts/Build

# Load in options file from pwd
source $RunPath/Options

# Update server
sudo apt-get update
sudo apt-get --yes upgrade

# Call scripts based on options file
if [ "$AddUsers" -eq 1 ]; then
  $RunPath/AddUsers.sh
else
  echo "Skipping user add"
fi

if [ "$IsWordpressAppliance" -eq 1 ]; then
  $RunPath/WordpressAppliance.sh
else
  echo "Skipping WordPress appliance install"
fi

if [ "$IsWebServer" -eq 1 ]; then
  $RunPath/InstallApache2.sh
else
  echo "Skipping Apache install"
fi

if [ "$InstallCertbot" -eq 1 ]; then
  $RunPath/InstallCertbot.sh
else
  echo "Skipping CertBot install"
fi

if [ "$IsDBServer" -eq 1 ]; then
  $RunPath/InstallMySQL.sh
else
  echo "Skipping MySQL install"
fi

if [ "$IsDevServer" -eq 1 ]; then
  $RunPath/devWP.sh
else
  echo "Skipping DevWP install"
fi
