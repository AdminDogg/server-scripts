#!/bin/bash

# File Name:    addUsers.sh
# Description:  Add hugo and ricky to new ToastCloud server
# Version:      1
# Auther:       AdminDogg
# Date:         11/01/18


#    _____             _    ___ _             _
#   |_   _|__  __ _ __| |_ / __| |___ _  _ __| |
#     | |/ _ \/ _` (_-<  _| (__| / _ \ || / _` |
#     |_|\___/\__,_/__/\__|\___|_\___/\_,_\__,_|
#                    | |__ _  _
#                    | '_ \ || |
#                    |_.__/\_, |
#      _      _       _    |__/_
#     /_\  __| |_ __ (_)_ _ |   \ ___  __ _ __ _
#    / _ \/ _` | '  \| | ' \| |) / _ \/ _` / _` |
#   /_/ \_\__,_|_|_|_|_|_||_|___/\___/\__, \__, |
#                                     |___/|___/


# This script adds Hugo and Ricky as users of the new server
# including home directories and group memberships.


# Add ricky and Hugo as new users without passwords
sudo adduser --disabled-password --ingroup admin --gecos "" ricky
sudo adduser --disabled-password --ingroup admin --gecos "" hugo


# change to the new login accounts and set up authentication
# ricky
sudo su ricky
cd /home/ricky
mkdir .ssh
chmod 700 .ssh
touch .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCDTKq5dIw+QtjNtIBpfZDjuBHFoOEqxwS+7gTgMlGB0lJDW3xk0nIH9FqYHJPCwJFGONEVsEuYhhWdtMoco/IJSk7t1X7kTf/vIXCRpmWp2vcv6OD6qq5L26BSNYf/j0vi8hNmcSVnr2NfyikGN0DpnTR/MmWuyvn30wxWCLnwaTATuWjmvdyBc/68JLEoxsHgMwTYtw7Kzn+oooRigvSo8gWOiAydIt/lPBrPfWhW7KGM1zXAiqTO6CsKkHY1wyy3R/075S2c4RLPznROdZWbXV3pzC6nuM+wZvgME+XhS+AxxYqpKfrdh0EUHLZ5o8aQS6aYJGJXwMruxL811W/J" >> .ssh/authorized_keys
exit

#Hugo
sudo su hugo
cd /home/hugo
mkdir .ssh
chmod 700 .ssh
touch .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCCy0fsKQsGKJ4dTS/go+3yL1GDoXvh9Y3z8UNNFkM3R+KgNiGhn4MIEx+7AHrI9QAPU+G5yBeCyJLCs1Ef2hMqSIN9ZJd9kiUeuWoV/ap7j8IZwxQF3bHE7tos9NFobXRduScD0khOWU8rrbPTesY5ckXBBNwoHMxucdppeDo+71F5EZDydLaliZyIudyWmopc/Fqv3QfsQ2mF+NtAYJ5NM69wQGhWs8ydIliBK5fm5agh3Y4O4MFbfm+EMt1d2M+sRoLr3YpvMcfOv7+6hyzfq1ET4B91SCe6cbZdozIFeBzmhDiKZVX0kye5Q0WtC+aeDURFejs90haGernwZ6rl" >> .ssh/authorized_keys
exit


# Test to see if www-data group exists and add users to it
if [[ $(getent group | grep www-data) ]]; then
  sudo usermod -a -G www-data ricky
  sudo usermod -a -G www-data hugo
else
  sudo groupadd www-data
  sudo usermod -a -G www-data ricky
  sudo usermod -a -G www-data hugo
fi
