#!/bin/bash

# File Name:    ServerBuildv1.sh
# Description:  Fresh ToastCloud server automated build
# Version:      2
# Auther:       AdminDogg
# Date:         11/01/18


#    _____             _    ___ _             _
#   |_   _|__  __ _ __| |_ / __| |___ _  _ __| |
#     | |/ _ \/ _` (_-<  _| (__| / _ \ || / _` |
#     |_|\___/\__,_/__/\__|\___|_\___/\_,_\__,_|
#                    | |__ _  _
#                    | '_ \ || |
#                    |_.__/\_, |
#      _      _       _    |__/_
#     /_\  __| |_ __ (_)_ _ |   \ ___  __ _ __ _
#    / _ \/ _` | '  \| | ' \| |) / _ \/ _` / _` |
#   /_/ \_\__,_|_|_|_|_|_||_|___/\___/\__, \__, |
#                                     |___/|___/

#Check all used variables to see if they have been set
if $DBName = ""; then
  echo "DBName not set"
  exit 1
fi

if $HostName = ""; then
  echo "HostName not set"
  exit 1
fi

# Wordpress database username
WPUser=WP_$HostName

# Wordpress database password
WPDBPassword=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32}`

# Set MySQL root password
MySQLrootPW=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32}`

# Add CertBot repository to apt
sudo add-apt-repository --yes ppa:certbot/certbot

# Set server hostname
OldHostname=$(cat /etc/hostname)

sudo sed -i "s/$OldHostname/$HostName/g" /etc/hosts
sudo sed -i "s/$OldHostname/$HostName/g" /etc/hostname

# Install Apache, php and Wordpress required php modules
sudo apt-get --yes install apache2 php libapache2-mod-php php-mcrypt php-mysql \
php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc

# Add the ServerName and .htaccess overrides to the Apache config file
echo "\n\nServerName $HostName\n\n" | sudo tee -a /etc/apache2/apache2.conf
echo "<Directory /var/www/html/>
    AllowOverride All
</Directory>" | sudo tee -a /etc/apache2/apache2.conf
sudo systemctl restart apache2

# Open enable Apache firewall rules
sudo ufw allow in "Apache Full"

# Amend apache configuration to prefer php files over html files
sudo sed -i "s/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm/g" /etc/apache2/mods-enabled/dir.conf
sudo systemctl restart apache2

# Install MySQL
export DEBIAN_FRONTEND=noninteractive
echo mysql-server mysql-server/root_password select $MySQLrootPW | sudo debconf-set-selections
echo mysql-server mysql-server/root_password_again select $MySQLrootPW | sudo debconf-set-selections
sudo apt-get install -y mysql-server

# Create Wordpress database
mysql -u root -p$MySQLrootPW -e "CREATE DATABASE $DBName DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL ON $DBName.* TO '$WPUser'@'localhost' IDENTIFIED BY '$WPDBPassword';
FLUSH PRIVILEGES;"

# Download and unpack Wordpress.  Set security, create configs and copy to Apache root
cd /tmp
curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
touch /tmp/wordpress/.htaccess
chmod 660 /tmp/wordpress/.htaccess
cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php
mkdir /tmp/wordpress/wp-content/upgrade
sudo cp -a /tmp/wordpress/. /var/www/html
sudo chown -R $User:www-data /var/www/html
sudo find /var/www/html -type d -exec chmod g+s {} \;
sudo chmod g+w /var/www/html/wp-content
sudo chmod -R g+w /var/www/html/wp-content/themes
sudo chmod -R g+w /var/www/html/wp-content/plugins

# Get new salts and plug them into WPConfig
## curl -s https://api.wordpress.org/secret-key/1.1/salt/ >> /tmp/salt

# Assign each key to a variable
## AUTH_KEY=$(cat /tmp/salt | sed -n '1p')
## SECURE_AUTH_KEY=$(cat /tmp/salt | sed -n '2p')
## LOGGED_IN_KEY=$(cat /tmp/salt | sed -n '3p')
## NONCE_KEY=$(cat /tmp/salt | sed -n '4p')
## AUTH_SALT=$(cat /tmp/salt | sed -n '5p')
## SECURE_AUTH_SALT=$(cat /tmp/salt | sed -n '6p')
## LOGGED_IN_SALT=$(cat /tmp/salt | sed -n '7p')
## NONCE_SALT=$(cat /tmp/salt | sed -n '8p')

## Debug, print variables to the screen
## echo $AUTH_KEY
## echo $SECURE_AUTH_KEY
## echo $LOGGED_IN_KEY
## echo $NONCE_KEY
## echo $AUTH_SALT
## echo $SECURE_AUTH_SALT
## echo $LOGGED_IN_SALT
## echo $NONCE_SALT

## sudo sed -i "s/define('AUTH_KEY',         'put your unique phrase here');/define('NONCE_SALT', 'define('AUTH_KEY', 'm}hkMxW93_^rYqt-zJa,<@z7WH@kR`z-vt2/p&+`tC>1-w`#?#uGw@-~3y!wggv@');/g" /var/www/html/wp-config.php

# Plug the keys from the variables into the wp-config file
## sudo sed -i "s/define('AUTH_KEY',         'put your unique phrase here');/$AUTH_KEY/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('SECURE_AUTH_KEY',         'put your unique phrase here');/$SECURE_AUTH_KEY/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('LOGGED_IN_KEY',         'put your unique phrase here');/$LOGGED_IN_KEY/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('NONCE_KEY',         'put your unique phrase here');/$NONCE_KEY/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('AUTH_SALT',         'put your unique phrase here');/$AUTH_SALT/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('SECURE_AUTH_SALT',         'put your unique phrase here');/$SECURE_AUTH_SALT/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('LOGGED_IN_SALT',         'put your unique phrase here');/$LOGGED_IN_SALT/g" /var/www/html/wp-config.php
## sudo sed -i "s/define('NONCE_SALT',         'put your unique phrase here');/$NONCE_SALT/g" /var/www/html/wp-config.php

# Plug database details into wp-config file
sudo sed -i "s/database_name_here/$DBName/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/$WPUser/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/$WPDBPassword/g" /var/www/html/wp-config.php

# Print passwords to the screen
printf "\n\n"
echo "The MySQL root password was set to: "
echo $MySQLrootPW

printf "\n############################\n\n"
echo "The Wordpress database user ($WPUser)password was set to: "
echo $WPDBPassword
printf "\n\n"

echo -n "Document these passwords and press [ENTER]"
read dummy

# Clear out variables for security and clear the screen
WPDBPassword=""
MySQLrootPW=""
clear

echo "Wordpress setup complete"
